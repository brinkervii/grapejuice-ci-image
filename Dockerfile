FROM debian:bookworm

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && \
  apt-get install -y \
  devscripts \
  debhelper \
  git \
  unzip \
  build-essential \
  pkg-config \
  findutils \
  python3 \
  python3-pip \
  python3-virtualenv \
  python3-venv \
  python3-certifi \
  python3-dbus \
  python3-packaging \
  python3-psutil \
  python3-urllib3 \
  python3-wget \
  python3-gi \
  libcairo2 \
  libcairo2-dev \
  libgirepository-1.0-1 \
  libgtk-3-0 \
  libgtk-3-bin \
  libdbus-1-3 \
  virtualenv \
  libcairo2-dev \
  libgirepository1.0-dev \
  libgtk-3-0 \
  libgtk-3-bin \
  libdbus-1-dev \
  gobject-introspection \
  gir1.2-gtk-3.0 \
  pipenv \
  xdg-utils \
  desktop-file-utils \
  fuse \
  libfuse2 \
  gnupg2 \
  expect \
  reprepro \
  wget \
  ca-certificates curl gnupg

RUN mkdir -p /etc/apt/keyrings && \
  echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list && \
  curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN apt-get update && apt-get install -y nodejs

RUN npm install -g pnpm@latest

COPY fake-wine.sh /usr/bin/wine
COPY fake-wine.sh /usr/bin/wine64

RUN chmod +x /usr/bin/wine && chmod +x /usr/bin/wine64
